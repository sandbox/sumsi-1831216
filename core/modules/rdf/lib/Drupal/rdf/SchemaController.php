<?

/**
 * @file
 * Definition of Drupal\rdf\SchemaController.
 */

namespace Drupal\rdf;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Resource controller for displaying entity schemas.
 */
class SchemaController {

  /**
   * Responds to a request for the schemas of all entities on a site.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function getEntitySchema() {
    $output = array();
    foreach (entity_get_info() as $entity_type => $entity_info) {
      $field_definitions = entity_get_controller($entity_type)->getFieldDefinitions(array(
        'entity_type' => $entity_type,
      ));
      $output[$entity_type] = $field_definitions;
    }
    return new Response(json_encode($output));
  }

  /**
   * Responds to a request for the schema of an entity type.
   *
   * @param string $entity_type
   *   The entity type. Defaults to NULL.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function getEntityTypeSchema($entity_type) {
    if (!$entity_info = entity_get_info($entity_type)) {
      throw new NotFoundHttpException(t('Entity type @entity_type not found', array('@entity_type' => $entity_type)));
    }
    $constraints = array('entity_type' => $entity_type);
    $output = entity_get_controller($entity_type)->getFieldDefinitions($constraints);
    return new Response(json_encode($output));
  }

  /**
   * Responds to a schema request for a bundle of a given entity type.
   *
   * @param string $entity_type
   *   The entity type. Defaults to NULL.
   * @param string $bundle
   *   The entity bundle. Defaults to NULL
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function getEntityBundleSchema($entity_type, $bundle) {
    if (!$entity_info = entity_get_info($entity_type)) {
      throw new NotFoundHttpException(t('Entity type @entity_type not found', array('@entity_type' => $entity_type)));
    }
    if (!array_key_exists($bundle, $entity_info['bundles'])) {
      throw new NotFoundHttpException(t('Bundle @bundle not found', array('@bundle' => $bundle)));
    }
    $output = entity_get_controller($entity_type)->getFieldDefinitions(array(
      'entity_type' => $entity_type,
      'bundle' => $bundle,
    ));
    return new Response(json_encode($output));
  }

}
